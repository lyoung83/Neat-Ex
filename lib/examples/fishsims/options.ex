defmodule Examples.FishSims.Options do
  @moduledoc false
  defstruct testAloneToo: true, size: 100, fishSpeed: 3.0, sharkSpeed: 4.0, sharkRadius: 10.0, sharkTurnAngle: :math.pi / 20, numOfSharks: 1, sharkDist: 48

  def new(opts \\ []) do
    Enum.reduce opts, %Examples.FishSims.Options{}, fn {k, v}, acc ->
      Map.put(acc, k, v)
    end
  end
end
