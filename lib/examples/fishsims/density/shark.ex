defmodule Examples.FishSims.Density.Sharks do
  @moduledoc false
  alias Examples.FishSims.Std
  alias Examples.FishSims.Density

  def new(opts), do: Std.Sharks.distribute(Density.Shark, opts)
end

defmodule Examples.FishSims.Density.Shark do
  @moduledoc false
  use Examples.FishSims.Shark
  alias Examples.FishSims.Std

  def new(opts), do: new(-opts.size/2 * 0.95, 0.0, opts)
  def char(_shark), do: "#"

  def step(shark, living, _world) do
    shark
      |> setHeading(living)
      |> Std.Shark.setVector
      |> Std.Shark.limitAngleChange(shark.opts.sharkTurnAngle)
      |> Std.Fish.move
  end
  def setHeading(shark, living) do
    {x, y} = densest(living, shark.opts.size)
    Std.Shark.setHeading(shark, x, y)
  end

  def densest(living, size), do: densest(living, :x, {0, size/4}, {0, size/4})
  defp densest(living, prop, {curSplitVal, mod}, otherSplit) when mod > 1 do
    # IO.puts "#{length living}, prop:#{prop}, val:#{curSplitVal}, mod:#{mod}"
    {lessSide, moreSide} = Enum.partition(living, fn fish -> Map.get(fish, prop) <= curSplitVal end)
    {remaining, operator} = Enum.max_by([{lessSide, &(&1-&2)}, {moreSide, &(&1+&2)}], fn {list, _} -> length(list) end)
    densest(remaining, swap(prop), otherSplit, {operator.(curSplitVal, mod), mod/2})
  end
  defp densest(_, :x, {x, _}, {y, _}), do: {x, y}
  defp densest(_, :y, {y, _}, {x, _}), do: {x, y}
  defp swap(:x), do: :y
  defp swap(:y), do: :x
end
