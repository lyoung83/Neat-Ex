defmodule Examples.FishSims.Shark do
  @moduledoc false
  defmacro __using__(_) do
    quote location: :keep do
      defstruct x: 0.0, y: 0.0, angle: nil, opts: nil, assign: %{}

      import Examples.FishSims.Fish
      alias Examples.FishSims.Utils
      alias __MODULE__

      def new(opts), do: new(opts.size/2 * 0.95, 0.0, opts)
      def new(x, y, opts) do
        %Shark{x: x, y: y, opts: opts}
      end
      def step(shark, living, _world) when length(living) == 0, do: shark

      def mapify(shark), do: Examples.FishSims.Std.Fish.mapify(shark)
      def char(_shark), do: "@"

      defoverridable [char: 1, mapify: 1, new: 1]
    end
  end
end
