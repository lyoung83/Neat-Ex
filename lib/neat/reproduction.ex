defmodule Neat.Reproduction do
  require Neat
  alias Neat.Utils
  alias Neat.Speciation

  @doc "Given Neat, a repopulated species list is returned. Reproduction is very configurable by changing Neat.Options."
  def repopulate(neat = %Neat{species: species, opts: opts}) do
    avg_fitness_sum = species |> Stream.map(fn {{_, avg_fitness, _}, _} -> avg_fitness end) |> Enum.sum
    new_members = Utils.async_species_map(species, fn {{_rep, avg_fitness, _tsi}, members} ->
      reproduce_times = trunc(Float.ceil((avg_fitness / avg_fitness_sum) * opts.population_size)) - length(members)
      if reproduce_times > 0 do
        Enum.map(1..reproduce_times, fn _ ->
          ann1 = Enum.at(members, :rand.uniform(length(members)) - 1)
          rand = :rand.uniform
          cond do
            rand < opts.interspecies_mating_chance ->
              {_rep, dif_members} = Enum.at(species, :rand.uniform(length(species)) - 1)
              breed(neat, ann1, Enum.at(dif_members, :rand.uniform(length(dif_members)) - 1))
            rand < opts.interspecies_mating_chance + opts.child_from_mutation_chance ->
              mutate(neat, Utils.deconstruct(ann1))
            :else ->
              breed(neat, ann1, Enum.at(members, :rand.uniform(length(members)) - 1))
          end
        end)
      else
        []
      end
    end)
    Map.put(neat, :species, Speciation.speciate(species, new_members, opts))
  end

  @doc "Breeds together two networks, given the Neat struct, then network 1, then network 2. If the networks happen to be identical, then the child is created from mutation rather than mating."
  def breed(neat, ann1, ann2) do
    if ann1 == ann2 do
      mutate(neat, Utils.deconstruct(ann1))
    else
      mate(ann1, ann2)
    end
  end

  @doc "Mates two networks, provided with their fitness. Takes two {ann, fitnes} pairs. The topology of the child matches that of the fitter parent. In cases where fitness is identical, topologies are merged. For all connection genes that exist in both parents (under the same id), the child picks from the two randomely (the weight will probably differ between parents)."
  def mate({ann1, fitness1}=pair1, {ann2, fitness2}=pair2) do
    if fitness1 != fitness2 do
      {{fitter, _}, {lesser, _}} = orderByFitness(pair1, pair2) #where ann1 is fitter
      Map.put fitter, :connections, Enum.reduce(fitter.connections, %{}, fn {id, conn}, acc ->
        Map.put(acc, id,
          (if Map.has_key?(lesser.connections, id), do: pickOne(conn, lesser.connections[id]), else: conn)
        )
      end)
    else
      Map.put ann1, :connections, Map.merge(ann1.connections, ann2.connections, fn _id, conn1, conn2 ->
        pickOne(conn1, conn2)
      end)
    end
  end
  defp orderByFitness({ann1, fitness1}, {ann2, fitness2}) when fitness1 > fitness2, do: {{ann1, fitness1}, {ann2, fitness2}}
  defp orderByFitness({ann1, fitness1}, {ann2, fitness2}), do: {{ann2, fitness2}, {ann1, fitness1}}
  defp pickOne(item1, item2) do
    if :rand.uniform() < 0.5, do: item1, else: item2
  end

  @doc "Mutates a given network, provided with the Neat struct, and the network. Returns the newly mutated network."
  def mutate(neat, ann) do
    opts = neat.opts
    rand = :rand.uniform()
    cond do
      rand < opts.new_node_chance ->
        mutate_new_node(neat, ann)
      rand < opts.new_node_chance + opts.new_link_chance ->
        mutate_new_link(neat, ann)
      :else ->
        mutate_weights(neat, ann)
    end
  end

  @doc "Performs a `new_node` mutation on a network, provided with the Neat struct, and the network. Returns the new network with the new node."
  def mutate_new_node(neat, ann) do
    enabled = Enum.filter(ann.connections, fn {_, conn} -> conn.enabled end)
    if length(enabled) == 0 do
      mutate_random_enable(neat, ann)
    else
      {conn_id, conn} = Enum.at(enabled, :rand.uniform(length(enabled)) - 1)
      node_id = Ann.Connection.genCantorId(conn.input, conn.output) + neat.node_id_mod
      conn1_id = Ann.Connection.genCantorId(conn.input, node_id) + neat.conn_id_mod
      conn2_id = Ann.Connection.genCantorId(node_id, conn.output) + neat.conn_id_mod
      Map.put(ann, :connections,
        ann.connections
          |> Map.put(conn_id, Map.put(conn, :enabled, false))
          |> Map.put(conn1_id, Map.put(conn, :output, node_id))
          |> Map.put(conn2_id, Map.put(Map.put(conn, :input, node_id), :weight, 1.0))
      )
    end
  end

  @doc "Performs a `new_link` mutation on a network, provided with the Neat struct, and the network. Returns the new network with the new link."
  def mutate_new_link(neat, ann) do
    opts = neat.opts
    inputNodes = Enum.reduce(ann.input, MapSet.new, fn input, acc -> MapSet.put(acc, input) end)
    outputNodes = Enum.reduce(ann.output, MapSet.new, fn output, acc -> MapSet.put(acc, output) end)
    knownNodes = MapSet.union(inputNodes, outputNodes)
    {allNodes, conns} = Enum.reduce(ann.connections, {knownNodes, MapSet.new},
      fn {_id, conn}, {allNodes, conns} ->
        {allNodes |> MapSet.put(conn.input) |> MapSet.put(conn.output), MapSet.put(conns, {conn.input, conn.output})}
      end
    )
    possibleOuts = MapSet.difference(allNodes, inputNodes) |> MapSet.to_list #an inputs node cannot be the output of a connection
    possibleIns = (if !opts.recurrent_nodes, do: MapSet.difference(allNodes, outputNodes), else: allNodes) |> MapSet.to_list #a connection input can't be an output node if recurrent_nodes is disabled.
    newConn = randSearch possibleIns, fn inNode ->
      randSearch possibleOuts, fn outNode ->
        newConn = {inNode, outNode}
        if (opts.recurrent_nodes || inNode != outNode) && (!MapSet.member?(conns, newConn)) do
          newConn
        else
          nil
        end
      end
    end
    if !newConn do
      mutate_new_node(neat, ann)
    else
      {inNode, outNode} = newConn
      Map.put(ann, :connections,
        Map.put(ann.connections, Ann.Connection.genCantorId(inNode, outNode), Ann.Connection.new(inNode, outNode, new_weight(opts)))
      )
    end
  end
  def randSearch([], _), do: nil
  def randSearch(list, fun) do
    {h, t} = popRand(list)
    fun.(h) || randSearch(t, fun)
  end
  def popRand(list) do
    i = :rand.uniform(length(list)) - 1
    {Enum.at(list, i), List.delete_at(list, i)}
  end

  @doc "Mutates all the weights in a network, provided with the Neat struct, and the network. Returns the newly mutated network."
  def mutate_weights(neat, ann) do
    opts = neat.opts
    Map.put(ann, :connections,
      Enum.reduce(ann.connections, %{}, fn {id, conn}, acc ->
        Map.put(acc, id,
          cond do
            :rand.uniform() < opts.new_weight_chance ->
              Map.put(conn, :weight, new_weight(opts))
            :else ->
              Map.put(conn, :weight, conn.weight + :rand.normal() * opts.weight_mutation_power)
          end
        )
      end)
    )
  end

  @doc "Randomley re-enables a connection in the network. Returns the newly mutated network."
  def mutate_random_enable(neat, ann) do
    disabled = Enum.filter(ann.connections, fn {_, conn} -> !conn.enabled end)
    if length(disabled) == 0 do
      IO.puts "No disabled links to enable, #{inspect(ann)}"
      mutate_new_link(neat, ann)
    else
      {conn_id, conn} = Enum.at(disabled, :rand.uniform(length(disabled)) - 1)
      conn = Map.put(conn, :enabled, true)
      Map.put(ann, :connections, Map.put(ann.connections, conn_id, conn))
    end
  end

  #@doc "Generates a new_weight given a Neat.Options. The weight is randomely distributed between :weight_range_min, and :weight_range_max."
  defp new_weight(opts) do
    :rand.uniform() * (opts.weight_range_max - opts.weight_range_min) + opts.weight_range_min
  end
end
