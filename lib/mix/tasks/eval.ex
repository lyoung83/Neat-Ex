defmodule Mix.Tasks.Eval do
  @moduledoc """
  Evaluates a nerual network.
  Usage: `mix eval {input1} [input2] [input3] [...]` for as many inputs as are needed.
  Once the command is issued, a prompt appears asking for the JSON for the neural network. Paste it in as one line, then hit enter.
  """
  use Mix.Task

  alias Ann.Simulation

  def run(inputs) do
    json = IO.gets "Paste in ANN JSON data (as one line, then hit enter): "
    ann = Ann.new(json)
    inputMap = Enum.zip(ann.input, Enum.map(inputs, &String.to_float/1))
    sim = Simulation.new(ann)
    {time, sim} = :timer.tc(fn -> Simulation.eval(sim, inputMap, 1000) end)
    IO.puts "\nEvaluated in #{sim.step} step(s), and #{time} microseconds."
    sim.data
      |> Map.take(ann.output)
      |> Enum.sort(fn {k1, _}, {k2, _} -> k1 > k2 end)
      |> Enum.each(fn {k, v} ->
        IO.puts "#{k} -> #{Float.round(v, 5)}"
      end)
  end
end
