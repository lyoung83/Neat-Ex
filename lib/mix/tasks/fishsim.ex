defmodule Mix.Tasks.Fishsim do
  @moduledoc """
  Usage: `mix fishsim [printEvery] [minutes] [file_to_record]`
  Runs a fishhy evolution process. Runs for the given number of minutes (default 60), and will print out ascii art, or record to the given file every `printEvery` generations, default is 1. Ascii art is used if file is not specified.
  """
  use Mix.Task
  alias __MODULE__
  alias Examples.FishSims
  alias Examples.FishSims.Options
  alias Examples.FishSims.Std
  alias Examples.FishSims.Density

  def parseArgs(args) do
    printEvery = String.to_integer(Enum.at(args, 0, "1"))
    mins = String.to_integer(Enum.at(args, 1, "60"))
    fileName = Enum.at(args, 2)
    {printEvery, mins, fileName}
  end

  def run(args, annModule, worldModules, opts) do
    {printEvery, mins, fileName} = parseArgs(args)
    neat = FishSims.run(mins, annModule, worldModules, opts, printEvery, fileName)
    {ann, _fitness} = neat.best
    Ann.saveJson(ann, "bestFish.json")
    Ann.saveGZ(ann, "bestFish.gz")
  end

  def run(args) do
    run(args, Std.Fish, %{sharks: Std.Sharks}, Options.new)
  end

  defmodule Fast do
    @moduledoc "Spawns slower Fish, and a faster Std.Shark."
    def run(args) do
      Fishsim.run(args, Std.Fish, %{sharks: Density.Sharks}, Options.new(fishSpeed: 2.0, sharkSpeed: 5.0, sharkTurnAngle: :math.pi / 5))
    end
  end

  defmodule Dense do
    @moduledoc "Spawns a Density.Shark."
    def run(args) do
      Fishsim.run(args, Std.Fish, %{sharks: Density.Sharks}, Options.new(fishSpeed: 3.0, sharkSpeed: 5.0, sharkTurnAngle: :math.pi / 5))
    end
  end

  defmodule Two do
    @moduledoc "Spawns a Std.Shark and a Density.Shark."
    def run(args) do#[Std.Shark, Density.Shark]
      Fishsim.run(args, Std.Fish, %{sharks: Std.Sharks}, Options.new(numOfSharks: 2))
    end
  end
end
