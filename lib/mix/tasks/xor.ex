defmodule Mix.Tasks.Xor do
  @moduledoc false

  @doc false
  def run(args), do: Mix.Tasks.Xor.Single.run(args)

  defmodule Single do
    @moduledoc "`mix xor.single` will run a single xor evolution, printing out information about the process, and the best network."
    use Mix.Task

    def run(_) do
      neat = Examples.Xor.run(true)
      {ann, _fitness} = neat.best
      IO.puts Ann.json(ann)
    end

    defmodule Profile do
      @moduledoc "`mix xor.single.profile` will run a single xor evolution, printing out profiling information."
      def run(_) do
        :eprof.profile(fn -> Examples.Xor.run(false, 15) end)
        :eprof.analyze
        # :fprof.apply(fn -> Examples.Xor.run(false, 15) end, [])
        # :fprof.profile()
        # :fprof.analyse(
        #   [
        #     callers: true,
        #     sort: :own,
        #     totals: true,
        #     details: true
        #   ]
        # )
      end
    end
  end

  defmodule Multi do
    @moduledoc "`mix xor.multi` runs multiple xor evolutions simultaneously, printing out the average number of generations taken."
    use Mix.Task

    def run(args) do
      number = String.to_integer(Enum.at(args, 0, "10"))
      genSum = 1..number
        |> Enum.map(fn _ -> Task.async(fn -> Examples.Xor.run(false) end) end)
        |> Enum.map(&Task.await(&1, 600000)) #10 minutes
        |> Enum.map(&(&1.generation))
        |> Enum.sum
      IO.puts "\nAverage number of generations: #{genSum / number}"
    end
  end
end
