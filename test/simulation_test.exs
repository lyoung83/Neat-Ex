defmodule SimulationTest do
  use ExUnit.Case
  alias Ann.Simulation

  setup do
    ann = Ann.new([1, 2, 3], [4], Ann.Connection.createFromList(
        [#{in, out, weight},
          {1, 5, 0.25},
          {2, 5, 0.75},
          {3, 5, -0.4},
          {5, 4, 0.25},
          {1, 4, 0.1},
          {5, 5, -0.2}
        ]
      )
    )
    {:ok, [ann: ann, sim: Simulation.new(ann)]}
  end

  test "Simulation", context do
    sim = context[:sim]
    input = %{1 => 0.25, 2 => -0.25, 3 => 0.5}
    sim = Simulation.eval(sim, input, 100, 0.001)
    output = sim.data[4]
    {correctStep, correctOut} = {8, -0.16897695932628584}
    msg = "evaluated #{sim.step} time(s), with output #{output}"
    assert sim.step == correctStep, "Step count was not #{correctStep}. Result: #{msg}"
    assert output == correctOut, "Output was not #{correctOut}. Result: #{msg}"
  end
end
