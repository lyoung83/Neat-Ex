defmodule GradAproxTest do
  use ExUnit.Case

  # doctest GradAprox

  test "GradAprox", _context do
    settings = {-5, 5, 0.001} #initial values range from -5 to 5, and will be modified by 0.001 for derivative approximation.
    {_params, data} = GradAprox.minimize %{x: settings, y: settings}, fn %{x: x, y: y} ->
      :math.sqrt(:math.pow(x - 18, 2) + :math.pow(y + 24, 2)) #distance from coordinates (18, -24)
    end, %{learn_val: 0.1, terminate?: fn _, info -> info.value < 0.1 end}
    IO.write "(GradAprox: #{data.step})"
  end
end
